= TAS Movie Editor =
 * written by: Maximus
 * contributors: gia, klmz
 * language: C#
 * compiles with: VS 2005, 2008, 2010

== Download == 
[http://code.google.com/p/tastools/downloads/detail?name=TAS-Movie-Editor-v0-12-2i.zip&can=2&q= TAS Movie Editor]

== Introduction ==

TAS Movie Editor is a program written to easily edit binary movie movie files (formats such as .fcm, .gmv, .vbm, etc).  

Supported formats:
 * SMV
 * FCM
 * GMV
 * FMV
 * VBM
 * M64
 * MMV
 * PXM
 * PJM (binary only)