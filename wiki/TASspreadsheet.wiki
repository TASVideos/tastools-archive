[http://dl-client.getdropbox.com/u/75355/HostedImages/TASspreadsheet.png]
----
*Direct download:*
  * [http://tastools.googlecode.com/files/TASspreadsheet.zip TASspreadsheet]

== Introduction ==

TASspreadsheet is an MS Excel workbook designed for recording framecounts and other TAS data for keeping a log of comparisons when making a TAS.  In addition it has worksheets data, tables, notes, RAM/ROM mapping, and version histories.

== Details ==

See Documentation sheet for details.